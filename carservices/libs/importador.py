from bs4 import BeautifulSoup
import requests


def get_info(ppu):
    info = {}
    req = requests.get(
        "https://soapweb.bciseguros.cl/web/DatosVehiculo?Convenios=700660&p=%s" % ppu)

    soup = BeautifulSoup(req.text, "html.parser")
    marca = soup.select("select[name$='Marca'] option:checked")
    if len(marca) > 0 and not "Seleccione" in marca[0].text:
        info["marca"] = marca[0].text
    
    modelo = soup.select("select[name$='Modelo'] option:checked")
    if len(modelo) > 0 and not "Seleccione" in modelo[0].text:
        info["modelo"] = modelo[0].text

    anno = soup.select("input[name$='Ano']")
    if len(anno) > 0:
        info["anno"] = anno[0]['value']
    return info
