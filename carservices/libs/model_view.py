from flask_admin.contrib.mongoengine import ModelView as MV
from flask_admin import BaseView as FlaskBaseView
from flask import redirect, url_for, request, flash, abort
from datetime import datetime
from flask_security import current_user


def date_time_formatter(view, context, model, name):
    if hasattr(model, name) and getattr(model, name):
        fecha = getattr(model, name)
        return u"%s-%s-%s" % (fecha.year, fecha.month, fecha.day)
    return ""

class ModelView(MV):
    can_export = False
    column_export_exclude_list = ['creado', 'actualizado', 'creado_por']
    export_types = ['csv', 'xls']
    column_formatters = {
        'creado': date_time_formatter,
        'actualizado': date_time_formatter
    }
    can_view_details = True
    column_labels = dict(
        creado="Creado",
        actualizado="Editado"
    )
    form_excluded_columns = ('creado', 'actualizado', 'creado_por')
    page_size = 40

    def on_model_change(self, form, model, is_created):
        if is_created:
            if hasattr(model, 'creado'):
                model.creado = datetime.now()
            if hasattr(model, 'actualizado'):
                model.actualizado = datetime.now()
            if hasattr(model, 'creado_por'):
                if current_user.is_authenticated:
                    model.creado_por = current_user.to_dbref()
        else:
            if hasattr(model, 'actualizado'):
                model.actualizado = datetime.now()
        return model

    def is_accessible(self):
        return (current_user.is_authenticated and current_user.has_role('administrador'))
    
    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            if current_user.is_authenticated:
                abort(403)
            else:
                flash("Debe iniciar sesión para continuar", "warning")
                return redirect(url_for('login_view', next=request.url))
    
    def get_save_return_url(self, model, is_created):
        return self.get_url('.details_view', id=model.id)
    

class BaseView(FlaskBaseView):
    def is_accessible(self):
        return (current_user.is_authenticated and current_user.has_role('administrador'))
