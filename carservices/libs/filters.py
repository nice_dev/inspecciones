import babel

def register_filters(app):
    app.jinja_env.filters['datetime'] = format_datetime
    app.jinja_env.filters['time'] = format_time


def format_datetime(value, _format='short_datetime', default=''):
    if not value:
        return default
    if _format == 'date':
        _format = "%d-%m-%Y"
    if _format == 'short_datetime':
        _format = "%m-%d-%Y %H:%M"
    elif _format =="full":
        _format = "%m-%d-%Y %H:%M:%S"
    return value.strftime(_format)


def format_time(value, _format='short_datetime', default=''):
    if not value:
        return default
    return value.strftime('%H:%M')


