from libs.model_view import ModelView, BaseView

from flask import redirect, url_for, request, flash, abort
from datetime import datetime, timedelta
from flask_security import current_user
from flask_admin import AdminIndexView, expose
from models.servicio import Custodia, Inspeccion, Retiro, Traslado
from models.vehiculo import Vehiculo
from wtforms import validators
from mongoengine.queryset.visitor import Q

import re


def date_time_formatter(view, context, model, name):
    if hasattr(model, name):
        fecha = getattr(model, name)
        if fecha:
            return u"%s-%s-%s" % (fecha.year, fecha.month, fecha.day)
    return ""


class ServiceView(ModelView):
    
    def create_form(self):
        form = super().create_form()
        if ('vehiculo') in request.args.keys():
            vh = Vehiculo.objects(id=request.args.get('vehiculo')).first()
            form.vehiculo.data = vh
        return form
    
    def get_save_return_url(self, model, is_created):
        if model.vehiculo:
            return self.get_url('vehiculo.details_view', id=model.vehiculo.id)
        return self.get_url('.details_view', id=model.id)

class CustodiaView(ServiceView):
    can_export = True
    create_template = 'parking/create.html'

    column_filters = ('ingreso', 'salida')
    form_excluded_columns = ModelView.form_excluded_columns + ('dias_en_acopio', )
    
    column_formatters = {
        'ingreso': date_time_formatter,
        'salida': date_time_formatter
    }
    column_list = ('empresa', 'bodega', 'vehiculo', 'ingreso', 'salida',
                   'dias_en_acopio', 'facturable',)
    
    def on_model_change(self, form, model, is_created):
        model = super().on_model_change(form, model, is_created)
        if model.ingreso and model.salida:
            model.calcular_acopio()
            model.save()
        if model.vehiculo:
            if is_created:
                model.vehiculo.add_history("Custodia",
                                           "Se crea una custodia al vehiculo",
                                           current_user.to_dbref())
            else:
                model.vehiculo.add_history("Custodia",
                                           "Se edita la custodia del vehiculo",
                                           current_user.to_dbref())
        return model


class RetiroView(ServiceView):
    pass

class InspeccionView(ServiceView):
    form_columns = ('empresa', 'estado', 'vehiculo', 'inspectores', 'fecha_programada',
                    'facturable', 'comentarios', 'direccion')

    create_template = 'inspection/create.html'

class VehiculoView(ModelView):
    details_template = 'vehicles/show.html'
    column_searchable_list = ('ppu', 'marca', 'modelo')
    can_delete = False
    form_excluded_columns = ModelView.form_excluded_columns + ('historial', )

    column_default_sort = ("actualizado", True)

    def on_model_change(self, form, model, is_created):
        form.ppu.data = re.sub(r'[\W_]+', '', form.ppu.data)
        if is_created:
            if self.model.objects(ppu=form.ppu.data).count() >= 1:
                raise validators.ValidationError(
                    'Ya existe un vehiculo con la ppu registrada!')
        super().on_model_change(form, model, is_created)


    @expose('/ppu/<ppu>')
    @expose('/ppu')
    def ppu(self, ppu):
        if ppu == '0':
            ppu = request.args.get('ppu')
        ppu = ppu.upper()
        ppu = re.sub(r'[\W_]+', '', ppu)
        vh = Vehiculo.objects(ppu=ppu).first()
        if vh:
            return redirect(url_for('.details_view', id=vh.id))
        if 6 <= len(ppu) <=8:
            return redirect(url_for('.create_view', ppu=ppu))    
        return redirect(url_for('.index_view'))
    
    def create_form(self):
        form = super().create_form()
        if ('ppu') in request.args.keys():
            form.ppu.data = request.args.get("ppu")
        return form
    
    @expose('/scrap/<ppu>')    
    def scrap(self, ppu):
        from libs import importador as imp
        data = imp.get_info(ppu)
        vh = Vehiculo.objects(ppu=ppu).first()
        if vh:
            for key in data.keys():
                if hasattr(vh, key):
                    setattr(vh, key, data[key])
            vh.add_history("VEHICULO",
                           "Hace scraping de información de vehículo",
                           current_user.to_dbref())

        return str(data)


    
    @expose('/')
    def index_view(self):
        # Grab parameters from URL
        view_args = self._get_list_extra_args()
        if view_args.search and len(view_args.search) >= 6:
            ppu = view_args.search.upper()
            ppu = re.sub(r'[\W_]+', '', ppu)
            obj = Vehiculo.objects(ppu=ppu).first()
            if obj:
                return redirect(url_for('.details_view', id=obj.id))
        return super().index_view()
    
    @expose('/preview_delete/<_id>', methods=('POST', 'GET'))
    def preview_delete(self, _id):
        vehiculo = self.get_one(_id)
        if request.method == "POST":
            msg = ""
            qty = Inspeccion.objects(vehiculo=vehiculo).delete()
            if qty:
                flash("{} Inspecciones relacionados eliminados".format(qty), "info")
            qty = Retiro.objects(vehiculo=vehiculo).delete()
            if qty:
                flash("{} Retiros relacionados eliminados".format(qty), "info")
            qty = Traslado.objects(vehiculo=vehiculo).delete()
            if qty:
                flash("{} Traslados relacionados eliminados".format(qty), "info")
            qty = Custodia.objects(vehiculo=vehiculo).delete()
            if qty:
                flash("{} Custodias relacionados eliminados".format(qty), "info")
            ppu = vehiculo.ppu
            vehiculo.delete()
            flash("Vehiculo {} eliminado".format(ppu), "success")
            if len(msg):
                flash(msg, "info")
            
            return redirect(self.get_url('.index_view'))

        data = list(Inspeccion.objects(vehiculo=vehiculo).all())
        data += list(Retiro.objects(vehiculo=vehiculo).all())
        data += list(Traslado.objects(vehiculo=vehiculo).all())
        data += list(Custodia.objects(vehiculo=vehiculo).all())
        return self.render("/vehicles/preview_delete.html",
                           data=data, model=vehiculo)


class UsuarioView(ModelView):
    can_delete = False
    form_columns = ('nombres', 'apellidos', 'telefono', 'email', 'password', 'roles')
    column_list = ('nombres', 'apellidos', 'telefono',
                   'email', 'active', 'roles', 'last_login_at')
    column_labels = dict(last_login_at='Último ingreso')

class BodegaView(ModelView):
    can_delete = False
    details_template = 'storage/details.html'

class MyServices(BaseView):
    @expose('/')
    def index(self):
        return self.render("/services/index.html")
    
class ReportsView(BaseView):
    
    @expose('/')
    def index(self):
        return self.render("/reports/index.html")
    
    def is_accessible(self):
        return (current_user.is_authenticated and current_user.has_role('administrador'))
    
    def generate_parking_data(self, from_date, to_date):


        total_ingresos = Custodia.objects(ingreso__gte=from_date,
                                          ingreso__lte=to_date).count()
        
        custodias = Custodia.objects(Q(ingreso__lte=to_date) & 
                                     (Q(salida__gte=from_date) |
                                      Q(salida=None) |
                                      Q(salida__exists=False))).all()
        data = {}
        dias_facturables = 0
        dias_no_facturables = 0
        for custodia in custodias:
            empresa = custodia.empresa;
            if not empresa in data:
                data[empresa] = {"custodias": [],
                                 "empresa": empresa,
                                 "dias_facturables": 0,
                                 "dias_no_facturables": 0}
            ingreso = from_date
            salida = to_date
            if custodia.ingreso > from_date:
                ingreso = custodia.ingreso
            if custodia.salida and custodia.salida < salida:
                salida = custodia.salida
            
            dias = (salida.date() - ingreso.date()).days + 1
            if custodia.facturable:
                dias_facturables += dias
                data[empresa]["dias_facturables"] += dias
            else:
                dias_no_facturables += dias
                data[empresa]["dias_no_facturables"] += dias
            # custodia.dias_en_acopio = 
            custodia.dias_a_facturar =  dias

            data[empresa]["custodias"].append(custodia)

            

        return {
            "from_date": from_date,
            "to_date": to_date,
            "total_ingresos": total_ingresos,
            "total_salidas": len(custodias),
            "dias_facturables": dias_facturables,
            "dias_no_facturables": dias_no_facturables,
            "data": data,

        }

    @expose('/parking')
    def parking_report(self):
        args = request.args
        to_date = args.get("to_date", str(datetime.now().date()))
        from_date = args.get("from_date", str((datetime.now() - timedelta(days=31)).date()))
        from_date = datetime.strptime(from_date, "%Y-%m-%d")
        to_date = datetime.strptime(to_date, "%Y-%m-%d")

        data = self.generate_parking_data(from_date, to_date)
        
        return self.render("/reports/parking.html", **data)
    
class IndexView(AdminIndexView):
    def is_accessible(self):
        return current_user.is_authenticated

    def _handle_view(self, name, **kwargs):
        if not self.is_accessible():
            if current_user.is_authenticated:
                # permission denied
                abort(403)
            else:
                # login
                return redirect(url_for('security.login', next=request.url))
    
    def count_parking_vehicles(self):
        return Custodia.objects(salida=None).count()

    def num_reviews_today(self):
        today = datetime.today().date()
        tomorrow = datetime.today() + timedelta(days=1)
        qty = Inspeccion.objects(Q(fecha_programada__gte=today) & Q(fecha_programada__lt=tomorrow)).count()
        return qty
    
    def num_pickup_today(self, days_delta=0):
        today = datetime.today().date() + timedelta(days=days_delta)
        tomorrow = datetime.today() + timedelta(days=1)
        qty = Retiro.objects(Q(fecha_programada__gte=today) & Q(
            fecha_programada__lt=tomorrow)).count()
        return qty
    
    def my_reviews_today(self, days_delta=0 ):
        today = datetime.today().date() + timedelta(days=days_delta)
        tomorrow = today + timedelta(days=1)

        qty = Inspeccion.objects(Q(fecha_programada__gte=today) &
                                 Q(fecha_programada__lt=tomorrow)).count()

        return qty
    def my_reviews_tomorrow(self):
        tomorrow = datetime.today().date() + timedelta(days=1)
        return self.my_reviews_today(tomorrow)

    def services_today(self):
        today = datetime.today().date()
        tomorrow = today + timedelta(days=1)

        query = Q(fecha_programada__gte=today) &  \
                Q(fecha_programada__lt=tomorrow)
        if not current_user.has_role("administrador"):
            query &= Q(inspectores__in=[current_user])
        
        data = list(Inspeccion.objects(query).all())
        data += list(Retiro.objects(query).all())
        data += list(Traslado.objects(query).all())
        
        return data
