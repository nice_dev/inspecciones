from libs.model_view import BaseView
from flask_admin import expose
from flask import request, flash, stream_with_context, Response
import csv
from models.servicio import estado_visita, Inspeccion, Retiro, Direccion, Comentario, Custodia
from flask_security import current_user
from models.usuario import Usuario

from models.cliente import Empresa
from models.vehiculo import Vehiculo
from dateutil import parser
import json
from mongoengine.queryset.visitor import Q

from datetime import datetime

class ToolsView(BaseView):

    @expose('/')
    def index(self):
        return self.render("/tools/index.html")
    
    @expose('/scrapper')
    def scrapper(self):
        def generate():
            from libs import importador as imp
            qty = 0

            vehiculos = Vehiculo.objects(Q(anno=None) |
                                         Q(anno__exists=False))
            yield "<h2>{} vehiculos</h3>".format(len(vehiculos))
            yield "<table class='table'>"
            yield "<thead><th>#</th><th>PPU</th><th>Marca</th><th>Modelo</th><th>Año</th></thead>"
            for vh in vehiculos:
                try:
                    qty += 1
                    yield "<tr><td>{}</td><td>{}</td>".format(qty, vh.ppu)
                    if len(vh.ppu) > 6:
                        yield "<td colspan=6>Fomato no valido</td></tr>"
                        continue
                    data = imp.get_info(vh.ppu)
                    for key in data.keys():
                        if hasattr(vh, key):
                            setattr(vh, key, data[key])
                    yield "<td>{}</td><td>{}</td><td>{}</td></tr>".format(data["marca"], data["modelo"], data["anno"])
                    vh.add_history("VEHICULO",
                            "Hace scraping de información de vehículo",
                            current_user.to_dbref())
                except Exception as e:
                    yield "<td colspan=6>{}<td></tr>".format(str(e))
            yield "</table><h3>{} vehiculos leidos</h3>".format(qty)
        return Response(stream_with_context(generate()))


    @expose('/migrar')
    def migrar(self):
        actualizados = 0
        no_actulizados = 0
        sin_cambios = 0
        inspecciones = Inspeccion.objects.all()
        retiros = Retiro.objects.all()
        custodias = Custodia.objects.all()
        for servicio in [inspecciones, retiros, custodias]:
            for obj in servicio:
                
                if obj.vehiculo and obj.vehiculo.empresa:
                    if not obj.empresa:
                        obj.empresa = obj.vehiculo.empresa
                        obj.save()
                        actualizados += 1
                    else:
                        sin_cambios += 1
                else:
                    no_actulizados += 1
        msg = "{} actualizados, {} NO Actualizados, {} sin cambios"
        msg = msg.format(actualizados, no_actulizados, sin_cambios)
        return msg

    @expose('/importer', methods=["GET", "POST"])
    def importer(self):
        estados = [x[1] for x in estado_visita]
        empresas = Empresa.objects.all()
        data = []
        if request.method == "POST":
            fecha = request.form.get("fecha")

            empresa = Empresa.objects.get(id=request.form.get("empresa"))
            table = json.loads(request.form.get("datos", []))
            inspectores_objects = Usuario.objects.all()
            servicios = 0
            filas = 0
            for row in table:
                if row[1]:
                    filas += 1
                    estado = row[0]
                    direccion = row[1]
                    comuna = row[2]
                    ppu = row[3].upper()
                    hora = row[4]
                    servicio = row[5].upper()
                    inspectores = row[6]
                    comentario_str = row[7]
                    ejecutivo = row[8]

                    
                    if not ppu or len(ppu) < 4:
                        ppu = "SIN PPU"

                    estado_id = estado_visita[0][0]
                    for es_vi in estado_visita:
                        if es_vi[1] == estado:
                            estado_id = es_vi[0]
                            break
                

                    direccion = Direccion(direccion=direccion, comuna=comuna)
                    
                    fecha_str = fecha
                    if hora:
                        fecha_str = "{} {}".format(fecha, hora)
                    fecha_programada = parser.parse(fecha_str)
                    
                    vehiculo = Vehiculo.objects(ppu=ppu).first()
                    if not vehiculo:
                        vehiculo = Vehiculo(ppu=ppu,
                                            actualizado=datetime.now(),
                                            creado_por=current_user.to_dbref())
                        vehiculo.save()
                    
                    comentario = Comentario(comentario=comentario_str, 
                                            creado_por=current_user.to_dbref())
                    if "INSPECCION" in servicio:
                        inspeccion = Inspeccion(vehiculo=vehiculo,
                                                empresa=empresa, direccion=direccion,
                                                fecha_programada=fecha_programada,
                                                estado=estado_id)
                        if comentario_str:
                            inspeccion.comentarios.append(comentario)

                        for inspector in inspectores_objects:
                            if inspector.email.upper() in inspectores:
                                inspeccion.inspectores.append(inspector)
                        inspeccion.save()
                        servicios += 1
                        
                    if "RETIRO" in servicio:
                        
                        retiro = Retiro(vehiculo=vehiculo,
                                        empresa=empresa, direccion=direccion,
                                        fecha_programada=fecha_programada,
                                        estado=estado_id)
                        if comentario_str:
                            retiro.comentarios.append(comentario)
                        
                        for inspector in inspectores_objects:
                            if inspector.email.upper() in inspectores:
                                retiro.inspectores.append(inspector)
                        retiro.save()
                        servicios += 1
            flash("{} servicios importados en {} filas".format(servicios, filas),
                  "success")
                    


                            
        
        return self.render("/tools/import_services.html", data=data,
                           estados=estados, empresas=empresas)
