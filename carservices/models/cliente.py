from db import db


# Create models
class Empresa(db.Document):
    __tablename__ = "empresas"


    nombre_corto = db.StringField(max_lenght=10)
    nombre = db.StringField(max_lenght=200)
    valor_inspeccion = db.FloatField()
    valor_retiro = db.FloatField()
    valor_traslado = db.FloatField()
    valor_dia_bodega = db.FloatField()


    def __str__(self):
        return self.nombre_corto

class Ejecutivo(db.Document):
    __tablename__ = "ejecutivos"
    
    
    nombres = db.StringField(max_lenght=100)
    apellidos = db.StringField(max_lenght=100)
    
    telefono = db.StringField(max_lenght=15)
    email = db.StringField(max_lenght=250)

    def __str__(self):
        return self.nombres + " - " + self.apellidos




# # Create models
# class ValorServicio(db.Model):
#     __tablename__ = "valores_servicio"

#     id = db.Column(db.Integer, primary_key=True)
#     fecha_inicio = db.Column(db.Date)

#     empresa_id = db.Column(db.Integer, db.ForeignKey('empresas.id'), nullable=False)
#     empresa = db.relationship('Empresa')

    
#     nombre_corto = db.Column(db.String(10))
#     nombre = db.Column(db.String(200))
#     valor_inspeccion = db.Column(db.Float)
#     valor_retiro = db.Column(db.Float)
#     valor_traslado = db.Column(db.Float)
#     valor_dia_bodega = db.Column(db.Float)

#     def __str__(self):
#         return self.nombre_corto
