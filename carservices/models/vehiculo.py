from db import db
from models.servicio import Custodia, Retiro, Inspeccion
from datetime import datetime

class HistoriaVehiculo(db.EmbeddedDocument):
    creado = db.DateTimeField(default=datetime.now)
    modulo = db.StringField(max_length=8)
    descripcion = db.StringField()
    usuario = db.ReferenceField('Usuario')



# Create models
class Vehiculo(db.Document):
    __tablename__ = "vehiculos"
    empresa = db.ReferenceField('Empresa')

    ppu = db.StringField(max_length=8)
    marca = db.StringField(max_length=100)
    modelo = db.StringField(max_length=100)
    anno = db.IntField()
    comentario = db.StringField()
    historial = db.ListField(db.EmbeddedDocumentField(HistoriaVehiculo),
                             default=[])

    creado_por = db.ReferenceField('Usuario')
    creado = db.DateTimeField(default=datetime.now)
    actualizado = db.DateTimeField()

    def __str__(self):
        return "{} - {} {}".format( self.ppu, self.marca, self.modelo or '')
    
    def add_history(self, module, description, user):
        self.historial.append(HistoriaVehiculo(modulo=module,
                                               descripcion=description,
                                               usuario=user))
        self.actualizado = datetime.now()

        self.save()

    def custodias(self):
        return Custodia.objects(vehiculo=self).all()
    
    def retiros(self):
        return Retiro.objects(vehiculo=self).all()
    
    def inspecciones(self):
        return Inspeccion.objects(vehiculo=self).all()
    
    # def traslados(self):
    #     return Custodia.objects(vehiculo=self).all()
