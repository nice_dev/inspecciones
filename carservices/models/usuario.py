from db import db
from sqlalchemy_utils import EmailType
from flask_security import UserMixin, RoleMixin


class Rol(db.Document, RoleMixin):
    name = db.StringField()
    descripcion = db.StringField()

    def __str__(self):
        return self.name

# Create models
class Usuario(db.Document, UserMixin):
    nombres = db.StringField(max_length=100)
    apellidos = db.StringField(max_length=200)
    
    telefono = db.StringField(max_length=200)
    email = db.StringField(max_length=255)
    password = db.StringField(max_length=260, verbose_name="Contraseña")

    active = db.BooleanField(default=True)
    confirmed_at = db.DateTimeField()
    last_login_at = db.DateTimeField()
    current_login_at = db.DateTimeField()
    last_login_ip =db.StringField()
    current_login_ip =db.StringField()
    login_count = db.IntField()
    need_change_password = db.BooleanField(default=True)

    roles = db.ListField(db.ReferenceField(Rol), default=[])

    def __str__(self):
        return self.nombres
