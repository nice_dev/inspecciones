from db import db
from datetime import datetime


tipo_bodega = [
    ('interna', 'Interna'),
    ('externa', 'Externa')
]

# Create models
class Bodega(db.Document):
    nombre = db.StringField(max_lenght=100)
    tipo = db.StringField(choices=tipo_bodega)
    direccion = db.StringField(max_lenght=500)
    lat = db.FloatField()
    lng = db.FloatField()
    
    meta = {'strict': False}
    def __str__(self):
        return self.nombre
