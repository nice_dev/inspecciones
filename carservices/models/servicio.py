from db import db
from datetime import datetime


# Create models
estado_visita = [
    ("PROGRAMADA", "Programada"),
    ("ACEPTADA", "Aceptada"),
    ("EN PROGRESO", "En Progreso"),
    ("REALIZADA", "realizada"),
    ("CANCELADA", "cancelada"),
    ("REPROGRAMADA", "reprogramada"),
]


class Direccion(db.EmbeddedDocument):
    comuna = db.StringField()
    direccion = db.StringField()
    lat = db.FloatField()
    lng = db.FloatField()
    
class Comentario(db.EmbeddedDocument):
    comentario = db.StringField()
    creado_por = db.ReferenceField('Usuario')
    creado = db.DateTimeField(default=datetime.now)

class Inspeccion(db.Document):
    empresa = db.ReferenceField('Empresa')

    estado = db.StringField(choices=estado_visita)
    inspectores = db.ListField(db.ReferenceField('Usuario'))

    fecha_programada = db.DateTimeField()
    fecha_visita = db.DateTimeField()
    direccion = db.EmbeddedDocumentField(Direccion)

    facturable = db.BooleanField(default=True)

    vehiculo = db.ReferenceField('Vehiculo')
    comentarios = db.ListField(db.EmbeddedDocumentField(Comentario))
    
    creado_por = db.ReferenceField('Usuario')
    creado = db.DateTimeField(default=datetime.now)
    actualizado = db.DateTimeField(default=datetime.now)


# Create models
class Retiro(db.Document):
    empresa = db.ReferenceField('Empresa')

    estado = db.StringField(choices=estado_visita)
    inspectores = db.ListField(db.ReferenceField('Usuario'))

    facturable = db.BooleanField(default=True)

    fecha_programada = db.DateTimeField()
    fecha_visita = db.DateTimeField()

    direccion = db.EmbeddedDocumentField(Direccion)


    comentario = db.StringField()
    comentarios = db.ListField(db.EmbeddedDocumentField(Comentario))

    vehiculo = db.ReferenceField('Vehiculo')

    creado_por = db.ReferenceField('Usuario')
    creado = db.DateTimeField(default=datetime.now)
    actualizado = db.DateTimeField(default=datetime.now)


# Create models
class Custodia(db.Document):
    empresa = db.ReferenceField('Empresa')

    bodega = db.ReferenceField('Bodega')
    vehiculo = db.ReferenceField('Vehiculo')
    
    ingreso = db.DateTimeField()
    salida = db.DateTimeField()
    odomentro = db.IntField(
        help_text="KM al momento de ingresar a almacenamiento")
    
    dias_en_acopio = db.FloatField()
    comentario = db.StringField()
    facturable = db.BooleanField(default=True)

    creado_por = db.ReferenceField('Usuario')
    creado = db.DateTimeField(default=datetime.now)
    actualizado = db.DateTimeField(default=datetime.now)

    meta = {'strict': False}

    def calcular_acopio(self, salida=None):
        if not salida and not self.salida:
            salida = datetime.now()
        elif not salida:
            salida = self.salida

        self.dias_en_acopio = (salida.date() - self.ingreso.date()).days + 1
        return self.dias_en_acopio


class Traslado(db.Document):
    empresa = db.ReferenceField('Empresa')

    inspectores = db.ListField(db.ReferenceField('Usuario'))

    vehiculo = db.ReferenceField('Vehiculo')
    fecha_programada = db.DateTimeField()

    origen = db.ReferenceField('Bodega')
    destino = db.ReferenceField('Bodega')
    direcion_origen = db.StringField()
    direcion_destino = db.StringField()
    comentarios = db.ListField(db.StringField())


    facturable = db.BooleanField(default=True)

    creado_por = db.ReferenceField('Usuario')
    creado = db.DateTimeField(default=datetime.now)
    actualizado = db.DateTimeField(default=datetime.now)
