from flask import Flask, request, session, redirect
import flask_admin as admin
from db import db
from flask_babelex import Babel
from flask_admin.base import MenuLink
from flask_mongoengine import MongoEngine


from flask_security import Security, MongoEngineUserDatastore, \
    UserMixin, RoleMixin, login_required

# modelos y vistas
from models.usuario import Usuario, Rol
from models.vehiculo import Vehiculo
from models.bodega import Bodega
from models.cliente import Empresa
from models.servicio import Inspeccion, Retiro, Custodia, Traslado
from libs.model_view import ModelView as MyModelView
from views.views import CustodiaView, IndexView, VehiculoView, UsuarioView, BodegaView, ReportsView, RetiroView, InspeccionView
from views.tools import ToolsView
from libs import filters


def create_app(config='config.cfg'):
    app = Flask(__name__)
    app.config.from_pyfile(config)
    db = MongoEngine(app)
    babel = Babel(app)
    # Setup Flask-Security
    user_datastore = MongoEngineUserDatastore(db, Usuario, Rol)
    security = Security(app, user_datastore)

    filters.register_filters(app)
    @babel.localeselector
    def get_locale():
        override = request.args.get('lang')
        if override:
            session['lang'] = override
        return session.get('lang', 'es')
    
    return app


app = create_app()



# Flask views
@app.route('/')
@login_required
def index():
    return redirect("/admin")



# Start app
# Create admin
admin = admin.Admin(app, name='carservice',
                    template_mode='bootstrap3', index_view=IndexView())
admin.add_view(VehiculoView(Vehiculo, "Vehiculos"))
admin.add_view(InspeccionView(Inspeccion, "Inspecciones", category="Servicios"))
admin.add_view(RetiroView(Retiro, "Retiros", category="Servicios"))
admin.add_view(RetiroView(Traslado, "Traslados", category="Servicios"))

admin.add_view(CustodiaView(Custodia, "Custodias", category="Servicios"))
admin.add_view(BodegaView(Bodega, "Bodegas", category="Mantenedores"))
admin.add_view(UsuarioView(Usuario, "Usuarios", category="Mantenedores"))
admin.add_view(MyModelView(Empresa, "Empresas", category="Mantenedores"))

admin.add_view(ReportsView("Reportes"))
admin.add_view(ToolsView("Herramientas", endpoint="tools"))

admin.add_link(MenuLink(name='Salir', endpoint='security.logout',
                        icon_type="glyph", icon_value="glyphicon-off"))

if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0", port=4000)
