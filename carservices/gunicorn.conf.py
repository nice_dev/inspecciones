
import multiprocessing
import os
import platform


if platform.system() == 'Linux':
    folder_log = '/var/log/inspecciones'
else:
    folder_log = 'logs/'

if not os.path.exists(folder_log):
    os.makedirs(folder_log)

accesslog = os.path.join(folder_log, 'access.log')
errorlog = os.path.join(folder_log, 'error.log')

bind = "127.0.0.1:5000"
workers = multiprocessing.cpu_count() * 2 + 1
print(f"corriendo con {workers} hilos")
loglevel = 'info'
timeout = 300
